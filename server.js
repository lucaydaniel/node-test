var express = require('express');

var app = express();

app.get('/', function(req, res){
  res.send('Hello World<br>Developed by <strong>Luca Daniel</strong>');
});

app.get('/about', function(req, res){
  res.send('My name is Luca Daniel I\'m a full stack developer');
});

app.get('/portfolio', function(req, res){
  res.send('Check my work at <a href="http://www.lucadaniel.com">lucadaniel.com</a>');
});

app.get('/contact', function(req, res){
  res.send('If you want to contact me feel free to reach out at <em>lucaydaniel@gmail.com</em>');
});


app.listen(3000);
console.log('Express started on port 3000');